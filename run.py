"""
Start main app.
"""
from drb import app

if __name__ == '__main__':
    app.run()
