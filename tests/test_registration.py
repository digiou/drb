import unittest
import json
from drb import app, db, mail


class RegistrationTestCase(unittest.TestCase):
    """Tests regarding the registration."""

    def setUp(self):
        """Define vars and init."""
        self.app = app
        self.client = self.app.test_client
        self.correct_data = json.dumps({"email": "correct@example.com", "name": "correct"})
        self.missing_name = json.dumps({"email": "missing_name@example.com"})
        self.missing_name_result = json.dumps({"reason": "Missing fields", "fields": ["name"]})
        self.missing_email = json.dumps({"name": "Missing Mail"})
        self.missing_email_result = json.dumps({"reason": "Missing fields", "fields": ["email"]})
        self.empty_data = json.dumps({})
        self.empty_data_result = json.dumps({"reason": "Missing fields", "fields": ["email", "name"]})
        self.duplicate_data = json.dumps({"email": "duplicate@example.com", "name": "duplicate"})
        self.duplicate_data_result = json.dumps({"fields": ["email"]})
        self.correct_data_email_send_test = json.dumps({"email": "correct2@example.com", "name": "correct2"})
        self.success_email_subject = "Successful registration from correct2@example.com"
        self.success_email_recipient = self.app.config['ADMIN_EMAIL']

        with self.app.app_context():
            db.create_all()

    def test_successful_creation(self):
        """Test API can create a new registration."""
        res = self.client().post('/register', data=self.correct_data, content_type='application/json')
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res.data.decode('utf-8'), self.correct_data)

    def test_only_post_allowed(self):
        """Test that only POST requests are allowed."""
        res = self.client().get('/register')
        self.assertEqual(res.status_code, 405)
        res = self.client().put('/register')
        self.assertEqual(res.status_code, 405)
        res = self.client().delete('/register')
        self.assertEqual(res.status_code, 405)
        res = self.client().patch('/register')
        self.assertEqual(res.status_code, 405)

    def test_missing_email(self):
        """Test result in case of missing email parameter."""
        res = self.client().post('/register', data=self.missing_email, content_type='application/json')
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.data.decode('utf-8'), self.missing_email_result)

    def test_missing_name(self):
        """Test result in case of missing name parameter."""
        res = self.client().post('/register', data=self.missing_name, content_type='application/json')
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.data.decode('utf-8'), self.missing_name_result)

    def test_empty_data(self):
        """Test result in case of missing parameters."""
        res = self.client().post('/register', data=self.empty_data, content_type='application/json')
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.data.decode('utf-8'), self.empty_data_result)

    def test_duplicate(self):
        """Test duplicate email case."""
        res = self.client().post('/register', data=self.duplicate_data, content_type='application/json')
        self.assertEqual(res.status_code, 201)
        self.assertEqual(res.data.decode('utf-8'), self.duplicate_data)
        res = self.client().post('/register', data=self.duplicate_data, content_type='application/json')
        self.assertEqual(res.status_code, 409)
        self.assertEqual(res.data.decode('utf-8'), self.duplicate_data_result)

    def test_email_send(self):
        """Test that email was sent to admin."""
        with mail.record_messages() as outgoing:
            res = self.client().post('/register', data=self.correct_data_email_send_test,
                                     content_type='application/json')
            self.assertEqual(res.status_code, 201)
            self.assertEqual(res.data.decode('utf-8'), self.correct_data_email_send_test)

            self.assertEqual(len(outgoing), 1)
            self.assertEqual(outgoing[0].subject, self.success_email_subject)
            self.assertListEqual(outgoing[0].recipients, [self.success_email_recipient])

    def tearDown(self):
        """Clean up and exit."""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
