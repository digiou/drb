from setuptools import setup

setup(
    name='drb',
    packages=['drb'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)