# DRB
A demo Flask-based REST endpoint that registers an email/name pair. No email confirmation. 

## First time setup
1. Export ENV with `. ./default_env_export.sh` from the `bin` directory.
    * In case of a new production setup, create a `production.cfg` file with all related info.
        * Also, create a new `ENVNAME_env_export.sh` and `source` that instead of the `default_env_export.sh` 
    * The app already defaults to `default.cfg` even if the `ENV` is not properly set for faster development (or laziness).
2. Install dependencies with `pip install -r requirements.txt`.
    * Make sure to use the appropriate Python, either default or from `virtualenv` for dev environments. 
3. Create a new `registrations` database on the running DB instance.
4. Start the DB migration scripts from the root folder of the project.
    * `python manage.py db init`
    * `python manage.py db migrate`
    * `python manage.py db upgrade`
    
## Running/restarting the project
1. Export ENV with `. ./default_env_export.sh`.
2. Start the dummy Python web server.
    * `python run.py`
    * Or use the web server of your choice and appropriate configuration.
    
## Running tests
1. Run `python -m unittest discover -s tests` or with your favorite test runner from your IDE/editor.

## Future improvements
* Use sample application behind an Nginx configuration since the built-in web server is not intended for production.
    
## Requirements
* Python 3.5+
* pip
