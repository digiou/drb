"""
The application models.
"""
import json
from flask import Response
from flask import request, jsonify
from flask_mail import Message
from sqlalchemy.exc import IntegrityError
from drb import app
from drb import db
from drb import mail
from drb.models import Registration


@app.route('/register', methods=['POST'])
def register():
    _content = request.get_json(silent=True)
    _missing_fields = []
    try:
        _email = _content['email']
    except KeyError:
        _missing_fields.append('email')

    try:
        _name = _content['name']
    except KeyError:
        _missing_fields.append('name')

    if len(_missing_fields) > 0:
        return Response(json.dumps(
            {"reason": "Missing fields", "fields": _missing_fields}), status=400, mimetype="application/json")

    try:
        reg = Registration(_email, _name)
        reg.save()

        msg = Message("Successful registration from {}".format(_email),
                      recipients=[app.config['ADMIN_EMAIL']])
        mail.send(msg)

        return Response(json.dumps({"email": reg.email, "name": reg.name}), status=201, mimetype="application/json")
    except IntegrityError:
        db.session().rollback()
        return Response(json.dumps({"fields": ["email"]}), status=409, mimetype="application/json")
