from drb import db


class Registration(db.Model):
    """This class represents the registration table."""

    __tablename__ = 'registrations'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(
        db.DateTime, default=db.func.current_timestamp(),
        onupdate=db.func.current_timestamp()
    )

    def __init__(self, email, name):
        """Initialize with email and name."""
        self.email = email
        self.name = name

    def __repr__(self):
        """Instance's string representation."""
        return '<id {}>'.format(self.id)

    def save(self):
        """Save the instance."""
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """Delete the instance, added for completeness."""
        db.session.delete(self)
        db.session.commit()
