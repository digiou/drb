import os
from flask import Flask
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy


config_path = os.getenv("DRB_SETTINGS", 'config/default.cfg')

app = Flask(__name__)
app.config.from_pyfile(config_path)

db = SQLAlchemy(app)

mail = Mail(app)

import drb.views
